#lang racket/base

(provide let-on)

(require "joiners.rkt"
         "../core.rkt"
         (for-syntax racket/base
                     syntax/parse))

(define-syntax let-on
  (lambda (stx)
    (syntax-parse stx
      [(_ ((resolved-id promise-expr)) body ...)
       #'(on promise-expr
             (lambda (resolved-id)
               body ...))]
      [(_ ((resolved-id promise-expr) ...) body ...)
       #'(on (all-of promise-expr ...)
             (lambda (all-resolved)
               (apply (lambda (resolved-id ...)
                        body ...)
                      all-resolved)))])))

(module+ test
  (require rackunit
           "../vat.rkt")
  (define a-vat (make-vat))
  
  (define ((^doubler bcom) x) (* x 2))
  (define doubler
    (a-vat 'spawn ^doubler))
  (define bob
    (a-vat 'spawn (lambda _ (lambda _ 'i-am-bob))))

  (define result1-ch
    (make-channel))
  (a-vat 'run
         (lambda ()
           (let-on ([x-double (<- doubler 3)])
             (channel-put result1-ch x-double))))
  (test-equal?
   "let-on with one var"
   (sync result1-ch)
   6)

  (define result2-ch
    (make-channel))
  (a-vat 'run
         (lambda ()
           (let-on ([x-double-3 (<- doubler 3)]
                    [x-double-9 (<- doubler 9)]
                    [bob (<- bob)])
             (channel-put result2-ch (list x-double-3 x-double-9 bob)))))
  (test-equal?
   "let-on with multiple vars"
   (sync result2-ch)
   '(6 18 i-am-bob)))
