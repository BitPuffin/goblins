#lang racket/base

;; Coroutines which are safe enough for Goblins' needs.
;; No actor on the stack called with $ can interrupt an actor below it.
;; Instead, actors suspending themselves with "await" suspend not to
;; the turn toplevel, but within themselves, returning a promise...
;; which, as it turns out, composes beautifully with promise pipelining.

(provide call-with-await with-await)

(require "../core.rkt"
         racket/match)

(define (call-with-await proc)
  (define await-prompt-tag (make-continuation-prompt-tag))
  (define (await-prompted thunk)
    (call-with-continuation-prompt
     thunk
     await-prompt-tag
     (lambda (k awaited-vow)
       (define (resume-awaited-fulfilled val)
         (await-prompted (lambda () (k `(fulfilled ,val)))))
       (define (resume-awaited-broken err)
         (await-prompted (lambda () (k `(broken ,err)))))
       (on awaited-vow resume-awaited-fulfilled
           #:catch resume-awaited-broken
           #:promise? #t))))
  (define (await awaited-vow)
    (define resume-result
      (call-with-composable-continuation
       (lambda (k)
         (abort-current-continuation await-prompt-tag k awaited-vow))
       await-prompt-tag))
    (match resume-result
      ;; Fulfilled!  So let's continue by returning val-or-err
      [(list 'fulfilled val) val]
      [(list 'broken err) (raise err)]))
  (await-prompted (lambda () (proc await))))

(define-syntax-rule (with-await await body ...)
  (call-with-await
   (lambda (await)
     body ...)))

(module+ test
  (require rackunit
           "bootstrap.rkt"
           "methods.rkt"
           "../vat.rkt")

  (define ((^alice bcom screen) bob)
    (call-with-await
     (lambda (await)
       ($ screen 'displayln "before")
       (define from-bob (await (<- bob "hello")))
       ($ screen 'displayln (format "after, returned: ~a" from-bob))
       (define from-bob2 (await (<- bob "hello2")))
       ($ screen 'displayln (format "after 2, returned: ~a" from-bob2))
       'alice-final-return)))

  (define ((^bob bcom [n 0]) heard)
    (bcom (^bob bcom (add1 n))
          (list 'i-am-bob heard n)))

  (define (^display-screen bcom #:really-display? [really-display? #f])
    (let next ([lines '()])
      (methods
       [(displayln str)
        (when really-display?
          (displayln str))
        (bcom (next (cons str lines)))]
       [(get-lines) (reverse lines)])))

  (define am (make-actormap))
  (define-actormap-run am-run! am)
  (define am-screen
    (am-run! (spawn ^display-screen)))
  (test-true
   "awaited actor returns promise"
   (local-promise?
    (am-run!
     (define alice (spawn ^alice am-screen))
     (define bob (spawn ^bob))
     ($ alice bob))))

  ;; TODO, but low priority: can get rid of this channel and reuse am
  ;; above via an actormap-churn! operation
  (define vat (make-vat))
  (define-vat-run vat-run vat)
  (define result-ch (make-channel))

  (vat-run
   (define screen
     (spawn ^display-screen))
   (define alice (spawn ^alice screen))
   (define bob (spawn ^bob))
   (on ($ alice bob)
       (lambda (alice-final-val)
         (channel-put result-ch
                      (list 'screen-lines ($ screen 'get-lines)
                            'alice-final-val alice-final-val)))))

  (test-equal?
   "Full run of await coroutine stuff behaves expectedly"
   (sync/timeout 1 result-ch)
   '(screen-lines
     ("before"
      "after, returned: (i-am-bob hello 0)"
      "after 2, returned: (i-am-bob hello2 1)")
     alice-final-val alice-final-return))

  (struct exn:fail:fail-like-this exn:fail ()
    #:transparent)

  (define caught-result-ch (make-channel))
  (vat-run
   (define ((^breaker bcom))
     (raise (exn:fail:fail-like-this "wargabarga" (continuation-marks #f))))
   (define ((^catcher bcom) breaker)
     (with-await await
       (with-handlers ([exn:fail:fail-like-this?
                        (lambda (e)
                          (channel-put caught-result-ch "Yep, expected failure"))])
         (await (<- breaker)))))
   (define breaker (spawn ^breaker))
   (define catcher (spawn ^catcher))
   ($ catcher breaker))

  (test-equal?
   "Await re-raises exceptions"
   (sync/timeout 1 caught-result-ch)
   "Yep, expected failure"))
