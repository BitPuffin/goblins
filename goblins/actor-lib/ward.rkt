#lang racket/base

(provide spawn-warding-pair
         ward
         enchant
         warden->ward-proc)

;; This module provides a "warding" mechanism... behind the ward is some
;; interesting behavior an actor might not quite everyone to have access
;; to.  For every warder there is an associated incanter who can break
;; through the magical barrier to access that behavior.
;;
;;  - spawn-warding-pair: returns two values (both actors) to its
;;    continuation, a warden (who protects behavior) and an incanter (who
;;    can poke through the ward to access that behavior).
;;
;;    The warden is not used directly, see `ward` defined below.
;;
;;    The incanter is an actor which is called with its first argument, a
;;    target actor whose warded behavior we want to access, and the
;;    remaining arguments are passed to the 
;; 
;;  - ward: Use WARDEN to protect BEHAVIOR.
;;
;;  - enchant: Set up a proxy object imbued with the powers of INCANTER
;;    to operate on TARGET.  Any messages sent to the returned enchanted
;;    actor will be automatically passed through the incanter to target.
;;
;; See also http://www.erights.org/history/joule/MANUAL.B17.pdf
;; for the inspiration for this pattern

(require "../core.rkt"
         "../utils/simple-sealers.rkt"
         racket/match)

;; Keyword arguments:
;;  - #:async?: If true, the incanter will use <- instead of $ to
;;    proxy messages sends
;;  - #:sealer-triplet: 
;;
;; Returns two values to its continuation:
;;  - a warden, to be used with the ward procedure below
;;  - an incanter, to access actors which have warded methods
(define (spawn-warding-pair #:async? [async? #f]
                            #:sealer-triplet [sealer-triplet #f])
  (define-values (seal unseal sealed?)
    (match sealer-triplet
      [(list seal unseal sealed?)
       (values seal unseal sealed?)]
      [#f
       (make-sealer-triplet 'ward)]))
  ;; When invoked, the warden returns either:
  ;;  - #f: if these are not arguments sealed by the sealer, or
  ;;  - (list kws kw-vals args): the unsealed arguments
  (define (^warden _bcom)
    (lambda (maybe-sealed-args)
      (and (sealed? maybe-sealed-args)
           (unseal maybe-sealed-args))))
  (define (^incanter _bcom)
    (define $/<-
      (if async? <- $))
    (make-keyword-procedure
     (lambda (kws kw-args target . args)
       ($/<- target (seal (list kws kw-args args))))))

  (values (spawn ^warden) (spawn ^incanter)))

;; Sets up a magical barrier using the powers of `warden` to restrict
;; access to `behavior`.
;; #:extends, if provided, is a fallback procedure.
;;
;; If #:async? is #t, this will result in any call to this warded
;; behavior will return a promise.
(define (_ward warden behavior
               #:extends [extends #f]
               #:async? [async? #f])
  (define (error-out)
    (error "Not sealed args and no extended behavior"))
  (make-keyword-procedure
   (match-lambda*
     [(list '() '() maybe-sealed-args)
      (define (after-maybe-unsealed maybe-unsealed)
        (match maybe-unsealed
          [(list kws kw-vals args)
           (keyword-apply behavior kws kw-vals args)]
          ;; Hm, didn't match the seal, so let's use the extended
          ;; methods if appropriate...
          [#f
           ;; package these back up
           (define args (list maybe-sealed-args))
           (if extends
               (keyword-apply extends '() '() args)
               (error-out))]))
      (if async?
          (on (<- warden maybe-sealed-args)
              after-maybe-unsealed
              #:promise? #t)
          (after-maybe-unsealed ($ warden maybe-sealed-args)))]
     ;; We still need to make this async if async? is #t
     ;; for consistency... unfortunately this can result in a lot of
     [(list kws kw-vals args ...)
      (define (apply-or-error . _whatever)
        (if extends
            (keyword-apply extends kws kw-vals args)
            (error-out)))
      ;; lazy way to set up and subscribe to an immediately resolved promise
      (if async?
          (on #t apply-or-error #:promise? #t)
          (apply-or-error))])))

(define (ward #:extends [extends #f]
              #:async? [async? #f]
              . warden-behaviors)
  (let lp ([warden-behaviors warden-behaviors])
    (match warden-behaviors
      [(list warden behavior rest ...)
       (_ward warden behavior
              #:extends (lp rest)
              #:async? async?)]
      ['() extends])))

(define (warden->ward-proc warden)
  (define (ward-proc warded-beh extends-beh)
    (ward warden warded-beh
          #:extends extends-beh))
  ward-proc)

;; Sets up an "incantified proxy" that always sends messages through
;; the incanter
(define (^incantified _bcom incanter target
                    #:async? [async? #f])
  (define $/<-
    (if async? <- $))
  (make-keyword-procedure
   (lambda (kws kw-vals . args)
     (keyword-apply $/<- kws kw-vals incanter target args))))

(define (enchant incanter target
                 #:async? [async? #f])
  (spawn ^incantified incanter target #:async? async?))

(module+ test
  (require "methods.rkt"
           "bootstrap.rkt"
           rackunit
           racket/contract)

  (define (^inbox bcom mailbox-name admin-warden)
    (define (make-main-beh mailbox-name messages)
      (define admin-methods
        (methods
         [(revoke)
          (bcom revoked-beh)]
         [(get-messages)
          messages]
         [(set-name new-name #:upcase? [upcase? #f])
          (bcom (make-main-beh (if upcase?
                                   (string-upcase new-name)
                                   new-name)
                               messages))]))
      (define public-methods
        (methods
         [(send-message msg)
          (bcom mailbox-name (make-main-beh msg messages))]
         [(mailbox-name) mailbox-name]))

      (ward admin-warden admin-methods
            #:extends public-methods))
    
    (define revoked-beh
      (lambda _ (error "revoked")))

    (make-main-beh mailbox-name '()))

  (define am (make-actormap))
  (define-actormap-run am-run am)

  (match-define (list inbox admin-incanter)
    (am-run
     (define-values (admin-warden admin-incanter)
       (spawn-warding-pair))
     (list (spawn ^inbox "My First Inbox" admin-warden)
           admin-incanter)))

  (check-equal?
   (actormap-peek am inbox 'mailbox-name)
   "My First Inbox")
  
  (test-exn
   "Can't just set the name without incanter"
   any/c
   (lambda ()
     (actormap-poke! am inbox 'set-name "A brand new name")))

  (test-not-exn
   "Can set the name through the incanter without erroring out"
   (lambda ()
     (actormap-poke! am admin-incanter
                     inbox 'set-name "New name"
                     #:upcase? #t)))

  (test-equal?
   "New name successfully set via incanter"
   (actormap-peek am inbox 'mailbox-name)
   "NEW NAME")

  (define some-other-incanter
    (actormap-run! am
                   (lambda ()
                     (define-values (_some-warden some-incanter)
                       (spawn-warding-pair))
                     some-incanter)))

  (test-exn
   "Can't just set the name without incanter"
   any/c
   (lambda ()
     (actormap-poke! am some-other-incanter
                     inbox 'set-name "A brand new name")))

  (define inbox-admin
    (actormap-run! am
                   (lambda () (enchant admin-incanter inbox))))

  (test-not-exn
   "Can set the name through the incantified proxy without erroring out"
   (lambda ()
     (actormap-poke! am inbox-admin
                     'set-name "Another new name"
                     #:upcase? #t)))

  (test-equal?
   "New name successfully set via incantified proxy"
   (actormap-peek am inbox 'mailbox-name)
   "ANOTHER NEW NAME")

  ;; allow for warding multiple things at once
  (define (^multitool bcom tool1-warden tool2-warden)
    (ward tool1-warden (lambda _ 'tool1)
          tool2-warden (lambda _ 'tool2)
          #:extends (lambda _ 'fallback)))
  (define-values (tool1-warden tool1-incanter)
    (am-run (spawn-warding-pair)))
  (define-values (tool2-warden tool2-incanter)
    (am-run (spawn-warding-pair)))

  (define mtool
    (am-run (spawn ^multitool tool1-warden tool2-warden)))

  (test-equal?
   "multi-ward test 1"
   (am-run ($ tool1-incanter mtool))
   'tool1)
  (test-equal?
   "multi-ward test 2"
   (am-run ($ tool2-incanter mtool))
   'tool2)
  (test-equal?
   "multi-ward fallback"
   (am-run ($ mtool))
   'fallback))
