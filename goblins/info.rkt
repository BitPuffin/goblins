#lang info

(define deps '("base" "crypto" "syrup" "pk"))
(define build-deps '("rackunit-lib"
                     "scribble-lib" "sandbox-lib"
                     "racket-doc"))
(define pkg-desc
  "A transactional, distributed actor model environment")
(define version "0.8")
(define pkg-authors '("cwebber"))
(define scribblings '(("scribblings/goblins.scrbl" (multi-page))))
