* Releases
** v0.8
*** Overview

This release focuses on major evolutions to the (in-progress) CapTP
protocol, and related work.  We also now have coroutines and some
conveniences to integrating Racket's synchronizable events with
Goblins' promises, simplifying integration with the rest of the world
of Racket's IO.

Lots of other changes too, read on!

*** Breaking changes

The interfaces for captp code completely changed... see the example in
the documentation to see how to update your code.

*** New features

**** CapTP and OCapN

Major changes came through Goblins' support for CapTP.

Additionally, we've started conversations about interoperability of
CapTP (and related layers) with the [[https://github.com/ocapn/ocapn][OCapN]] project.  More on that in
the future!

***** Handoffs!  Handoffs!!!

The biggest feature for this version is that... we have "three party
handoffs"!

The key idea here is that since CapTP uses efficient pairwise integers
as live capability references between two machines.  So what happens
when Alice on machine A wants to introduce Bob on machine B to Carol
on machine C?  We now have a fancy "handoff" system using certificates
that set up the connection and do the handoff for you.

This is abstracted so that users, in general, don't need to worry
about the handoff machinery... it should (hopefully) just work!

***** New abstraction: netlayers!

CapTP is agnostic to what particular network abstraction it runs over.

Traditionally, the network abstraction layer has been called "VatTP"
in CapTP type systems; the rename to "netlayer" reflects that there
are multiple layers that can fulfill this role.  In this sense, a
netlayer is one that follows an abstracted "netlayer interface".

***** New netlayer: "onion" for Tor onion services

Relatedly, we now have a Tor Onion Services netlayer that ships with
Goblins.  Goblins can also help set up a new Onion service for you,
for easy peer to peer connections.

See the captp section of the documentation for an example of use.

***** New netlayer: "fake" for localized registry of fake machine addresses

This is useful for unit tests.  It's not well documented yet
unfortunately.  See [[file:./goblins/ocapn/netlayer/fake-intarwebs.rkt][fake-intarwebs.rkt]]'s unit tests for an example.

Yes, we might rename this module in the future.

***** URIs and corresponding structures for machines, sturdyrefs, etc

New URI structures, and corresponding Racket structs, have been
defined to facilitate both bootstrapping connections between CapTP
systems (including in handoffs).  At the moment, two are defined:

For machine type URIs:

: ocapn:m.<transport>.<transport-address>[.<transport-hints>]

For swissnum URIs:

: ocapn:s.<transport>.<transport-address>/<swiss-num>

And some examples of each:

: ocapn:m.onion.wy46gxdweyqn5m7ntzwlxinhdia2jjanlsh37gxklwhfec7yxqr4k3qd
: ocapn:s.onion.abpoiyaspodyoiapsdyiopbasyop/3cbe8e02-ca27-4699-b2dd-3e284c71fa96

See =goblins/ocapn/structs-urls.rkt= for more information.

**** actor-lib

Various additions and changes have happened to the =goblins/actor-lib=
submodules.  Proper documentation is still, well, mostly needed on
most of these, will hopefully come in the next release.  Patches
welcome. ;)

***** coroutines support: await

The new =goblins/actor-lib/await= submodule brings coroutines, at
last, to Goblins, in a way that's reasonably safe!  Well, as safe as
coroutines get anyway.

These particular coroutines compose nicely with Goblins' support of
promise pipelining.  =call-with-await= takes a thunk, and =with-await=
provides some macro'y syntax sugar.

Usage example:

#+BEGIN_SRC racket
(define ((^alice bcom screen) bob)
  (call-with-await
   (lambda (await)
     ($ screen 'displayln "before")
     (define from-bob (await (<- bob "hello")))
     ($ screen 'displayln (format "after, returned: ~a" from-bob))
     (define from-bob2 (await (<- bob "hello2")))
     ($ screen 'displayln (format "after 2, returned: ~a" from-bob2))
     'alice-final-return)))
#+END_SRC

***** actor-lib: sync/pr

New module in =goblins/actor-lib/sync-pr= that provides =sync/pr=,
which transforms Racket synchronizable events into Goblins promises,
making integrating all sorts of Racket I/O systems with Goblins much
easier!

***** actor-lib: sleep/pr

New module in =goblins/actor-lib/sleep-pr= which provides the
procedure =sync/pr=, which generates a promise that is fulfilled after
a period of time, similar to the =sleep= procedure.
      
***** actor-lib: common

 - Added =^incrementer=, a simple incrementing counter
 - added a ='remove= method for hashtable classes

***** define-actor: new sugar for defining actors

Experimental new syntax for defining actors that may be simpler, uses
macros.  This might be the preferred future way of defining actors,
but the macro-free version will always remain available.

***** simple-mint: a minimalist fiat currency example

=goblins/actor-lib/simple-mint= provides a simple example of a bank
with support for distributed accounts, mostly a demonstrative port of
the code from [[http://erights.org/elib/capability/ode/index.html][Capability-based Financial Instruments]].

**** Added a Makefile

For convenience, there is now a Makefile with some helpful make commands
(eg: compile, check, clean)

*** Breaking changes

**** on's #:regardless is #:finally again

=on='s =#:regardless= keyword has been named back to =#:finally=.

**** CapTP stuff

As said previously, the CapTP interface fully changed.  Check the docs
on how to use it.  (It may change further yet!)

**** ward =#:async-warden?= keyword renamed to =#:async?=

*** Bugfixes

**** Don't recalculate #:extends every time for methods

Previously, whatever code was in #:extends would be recalculated on every
invocation.  Yikes on performance and behavior both.  This is fixed.
